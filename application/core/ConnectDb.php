<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/12/18
 * Time: 7:53 PM
 */

namespace application\core;

use \PDO;
use \PDOException;

class ConnectDb {
    // Hold the class instance.
    private static $instance = null;
    private $conn;

    private $host = 'localhost';
    private $name = 'foa_1';
    private $pass = '11111111';
    private $user = 'root';

    // The db connection is established in the private constructor.
    private function __construct()
    {
        $this->conn = new PDO("mysql:host={$this->host}; dbname={$this->name}", $this->user,$this->pass,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new ConnectDb();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->conn;
    }
}
