<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/10/18
 * Time: 3:40 PM
 */

namespace application\controller;

use application\core\Controller;
use application\models\Gallery;
use application\models\User;


class AccountController extends Controller
{

    public function indexAction() {
        if (User::checkLogin()) {
            if (isset($_POST['submit'])) {
                $options['owner_id'] = User::getUserId();
                $options['name_img'] = $_POST['name'];
                $options['description'] = $_POST['description'];

                $id = Gallery::addPhoto($options);

                if ($id) {
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/public/img/{$id}.jpg");
                    }
                };
                $this->view->redirect('gallery/');
            }
            $this->view->render('Camagru | Account');
            return true;
        }
        $this->view->redirect('/');
        return true;
    }

    public function registerAction()
    {
        if(User::checkLogin() == false){
             if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['psw'])
                && isset($_POST['pswrepeat']) && isset($_POST['name'])) {
                 $user['name'] = $_POST['name'];
                 $user['email'] = $_POST['email'];
                 $user['password'] = $_POST['psw'];
                 if (!User::checkUserData($user['email'], $user['password'])) {
                     $userId = User::registerUser($user);
                     User::auth($userId);
                     $this->view->redirect('/');
                     return true;
                 }
             }
            $this->view->render('Camagru | Regiser');
            return true;
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

}