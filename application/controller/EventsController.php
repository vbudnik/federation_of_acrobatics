<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 12/27/18
 * Time: 8:24 PM
 */

namespace application\controller;


use application\core\Controller;

class EventsController extends Controller
{
    public function indexAction() {
        $this->view->render('ФСАУ | Змагання');
        return true;
    }
}