<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/15/18
 * Time: 8:35 PM
 */

namespace application\controller;


use application\core\Controller;
use application\core\Db;
use application\core\Pagination;
use application\models\Gallery;
use application\models\User;


class GalleryController extends Controller
{

    public function indexAction() {
        $this->view->render('ФСФУ | Галерея');
        return true;
    }

    public function photoAction() {
        $this->view->render('ФСФУ | Фото');
        return true;
    }

    public function videoAction() {
        $this->view->render('ФСФУ | Фото');
        return true;
    }
    
}
