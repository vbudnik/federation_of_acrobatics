<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 12/27/18
 * Time: 7:28 PM
 */

namespace application\controller;


use application\core\Controller;

use application\core\ConnectDb;
use application\core\Pagination;
use application\models\News;
use application\models\User;

class NewsController extends Controller
{
    public function indexAction() {

        $news = News::getAllNews($this->route);

        $lastId = News::getLastId();


        $pagination = new Pagination($this->route, $lastId);

        $vars = [
            'news' => $news,
            'pagination' => $pagination->get(),
        ];

         $this->view->render('ФСАУ | Новини', $vars);
        return true;
    }

    public function pageAction() {

        $newsId = $this->route['id'];

        $newsInformation = News::getOneNews($newsId);

        $vars = $newsInformation;

        $this->view->render('ФСАУ | Новини', $vars);
        return true;
    }
}