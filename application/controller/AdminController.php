<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 15.12.2018
 * Time: 17:55
 */

namespace application\controller;


use application\core\Controller;
use application\models\News;
use application\models\User;

class AdminController extends Controller
{
    public function indexAction() {
        $this->view->render('ФСФУ | Адмін панель');
        return true;
    }

    public function eventsAction() {
        $this->view->render('ФСФУ | Управління подіями');
        return true;
    }

    public function newsAction() {
        if (isset($_POST['submit']) && isset($_POST['name']) && isset($_POST['text_news'])) {
            $options['title'] = $_POST['name'];
            $options['description'] = $_POST['description'];
            $options['text_news'] = $_POST['text_news'];
            $options['photo'] = News::getLastId() + 1;

            $id = $options['photo'];


            if ($id) {
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/public/images/news/{$id}.jpg");
                }
            }
            if (News::addNews($options))
            {
                $this->view->redirect('/admin');
                return true;
            }
        }

        $vars = News::getAllInformationNews();

        $this->view->render('ФСФУ | Управління новинами', $vars);
        return true;
    }

    public function trenerAction() {
        if(isset($_POST['submit']) && isset($_POST['name'])){
            $options['name'] = $_POST['name'];
            $options['description'] = $_POST['description'];
            $options['photo'] = User::getLastIdTrener() + 1;

            $id = $options['photo'];

            if ($id) {
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/public/images/trener/{$id}.jpg");
                }
            }
            if (User::addTrener($options))
            {
                $this->view->redirect('/admin');
                return true;
            }
        }
        $this->view->render('ФСФУ | Добавити тренира');
        return true;
    }

    public function newsupdateAction() {

        $newsId = $this->route['id'];

        if (isset($_POST['submit'])){
            $options['title'] = $_POST['name'];
            $options['description'] = $_POST['description'];
            $options['text_news'] = $_POST['text_news'];
            $options['photo'] = News::getLastId() + 1;

            $id = $options['photo'];

            if ($id) {
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/public/images/news/{$id}.jpg");
                }
            }
            News::updateNews($options, $newsId);
            $this->view->redirect('/admin/news');
            return true;
        }

        $newsInformation = News::getOneNews($newsId);

        $vars = $newsInformation;

        $this->view->render('ФСФУ | Редагувати новину', $vars);
        return true;
    }

    public function newsdeleteAction() {

        $newsId = $this->route['id'];

        if(isset($_POST['submit']))
        {
            News::deleteNews($newsId);
            $this->view->redirect('/admin/news');
            return true;
        }


        $this->view->render('ФСФУ | Видалити новину');
        return true;

    }
}