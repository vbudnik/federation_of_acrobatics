<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 9:01 PM
 */

namespace application\controller;

use application\core\Controller;

use application\core\Pagination;
use application\core\PaginationTrener;
use application\models\Main;
use application\models\News;
use application\models\User;


class MainController extends Controller
{
    public function indexAction() {
        $route = 1;
        $news = News::getAllNews($route);

        $vars = [
            'news' => $news,
        ];

        $this->view->render('ФСАУ | Головна', $vars);
        return true;
    }

    public function kerivnitstvoAction() {
        $this->view->render('ФСАУ | Керівництво');
        return true;
    }

    public function documentationAction() {
        $this->view->render('ФСАУ | Документація');
        return true;
    }

    public function partnerAction() {
        $this->view->render('ФСАУ | Партнери');
        return true;
    }


    public function uspihiAction() {
        $this->view->render('ФСАУ | Успіхи');
        return true;
    }

    public function zbirnaAction() {
        $this->view->render('ФСАУ | Збірна');
        return true;
    }

    public function vitanyAction() {
        $this->view->render('ФСАУ | Вітання');
        return true;
    }

    public function contactAction() {
        $this->view->render('ФСАУ | Контакти');
        return true;
    }

}
