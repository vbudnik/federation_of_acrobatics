<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 8:20 PM
 */

return [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'kerivnitstvo' => [
        'controller' => 'main',
        'action' => 'kerivnitstvo',
    ],


    'documentation' => [
        'controller' => 'main',
        'action' => 'documentation',
    ],

    'partner' => [
        'controller' => 'main',
        'action' => 'partner',
    ],

    'events' => [
        'controller' => 'events',
        'action' => 'index',
    ],

    'uspihi' => [
        'controller' => 'main',
        'action' => 'uspihi',
    ],

    'zbirna' => [
        'controller' => 'main',
        'action' => 'zbirna',
    ],

    'vitany' => [
        'controller' => 'main',
        'action' => 'vitany',
    ],

    'contact' => [
        'controller' => 'main',
        'action' => 'contact',
    ],

    'gallery'  => [
        'controller' => 'gallery',
        'action' => 'index',
    ],

    'gallery/photo'  => [
        'controller' => 'gallery',
        'action' => 'photo',
    ],

    'gallery/video'  => [
        'controller' => 'gallery',
        'action' => 'video',
    ],

    // admin route

    'admin'  => [
        'controller' => 'admin',
        'action' => 'index',
    ],

    'admin/news'  => [
        'controller' => 'admin',
        'action' => 'news',
    ],

    'admin/events'  => [
        'controller' => 'admin',
        'action' => 'events',
    ],

    'admin/trener' => [
        'controller' => 'admin',
        'action' => 'trener',
    ],

//    for update news

    'newsupdate/{id:\d+}' => [
        'controller' => 'admin',
        'action' => 'newsupdate',
    ],

//    for delete news

    'newsdelete/{id:\d+}' => [
        'controller' => 'admin',
        'action' => 'newsdelete',
    ],

    //routs for trenit

    'trenir/{id:\d+}' => [
        'controller' => 'trenir',
        'action' => 'index',
    ],

    'people/{id:\d+}' => [
        'controller' => 'trenir',
        'action' => 'trenir',
    ],

    //routs for news page

    'news' => [
        'controller' => 'news',
        'action' => 'index',
    ],

    'news/{id:\d+}' => [
        'controller' => 'news',
        'action' => 'index',
    ],

    'page/{id:\d+}' => [
        'controller' => 'news',
        'action' => 'page',
    ],

];
