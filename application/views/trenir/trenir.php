<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-center">

                <div class="product-details"><!--product-details-->
                    <div class="col-sm-4">
                        <div class="view-product">
                            <img src="/public/images/trener/<?php echo $vars['photo'];?>.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="product-information"><!--/product-information-->
                            <h2><?php echo $vars['name'];?></h2>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->
                <div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#information" data-toggle="tab">Інформація</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="information" >
                            <div class="col-sm-9">
                                <div class="product-image-wrapper">
                                    <div class="productinfo text-left">
                                        <p><?php echo $vars['description'];?></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/category-tab-->
            </div>
        </div>
    </div>
</section>

