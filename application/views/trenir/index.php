<section>
    <div class="container">
        <div class="features_items"><!--features_items-->
            <h2 class="title text-center">Тренeра</h2>
            <?php foreach ($vars['trenera'] as $trener => $value): ?>
                <div class="col-sm-4">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="../public/images/trener/<?php echo $value['photo'];?>.jpg" alt="" />
                                <p><?php echo $value['name'];?></p>
                                <a href="/people/<?php echo $value['id'];?>" class="btn btn-default add-to-cart"><i class="fa"></i>Детальніше</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!--features_items-->
        <div class="clearfix">
            <?php echo $vars['pagination']; ?>
        </div>
    </div>
</section>

