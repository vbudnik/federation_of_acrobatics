<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#slider-carousel" data-slide-to="1"></li>
                        <li data-target="#slider-carousel" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <h2>Федерація Спортивної Акробатики України</h2>
                            </div>
                            <div class="col-sm-6">
                                <img src="../public/images/slider/1.jpg" class="girl img-responsive" alt="" />
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-6">
                                <h2>Федерація Спортивної Акробатики України</h2>

                            </div>
                            <div class="col-sm-6">
                                <img src="../public/images/slider/2.jpg" class="girl img-responsive" alt="" />
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-sm-6">
                                <h2>Федерація Спортивної Акробатики України</h2>
                            </div>
                            <div class="col-sm-6">
                                <img src="../public/images/slider/3.jpg" class="girl img-responsive" alt="" />
                            </div>
                        </div>

                    </div>

                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-right">

                <div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tsil" data-toggle="tab">Ціль</a></li>
                            <li><a href="#blazers" data-toggle="tab">Задачі</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tsil" >
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="productinfo text-left">
                                        <!-- <img src="images/home/gallery1.jpg" alt="" /> -->
                                        <p>Батьки повинні чітко розуміти, яких результатів очікують від дитини, коли приводять її на акробатику. Як вид спорту, вона дуже складна для дитини, до того ж дуже унікальна. Медики відзначають, що тренування з акробатики включають відразу кілька рішень для зростаючого дитячого організму. У маленького акробата буде прекрасна фізична підготовка, при якій навантаження розподіляється так, щоб задіяти всі групи м'язів.</p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="blazers" >
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <!-- <img src="images/home/gallery4.jpg" alt="" /> -->
                                            <p>Среди самых больших заслуг Вики —  серебро и две бронзы Универсиады в Казани в 2013 году (групповые упражнения), серебро и бронза Чемпионатов Европы 2013 и 2015 (команда), а также 4 бронзы с Чемпионатов мира: 2011 — командные соревнования, 2013 – групповое первенство, 2014 и 2015 — командные соревнования.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/category-tab-->

                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Новини</h2>
                    <?php foreach ($vars['news'] as $news => $value): ?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="../public/images/news/<?php echo $value['photo'];?>.jpg" alt="" />
                                        <p><?php echo $value['titel'];?></p>
                                        <div class="post-meta">
                                            <ul>
                                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                            </ul>
                                        </div>
                                        <a href="/page/<?php echo $value['id'];?>" class="btn btn-default add-to-cart"><i class="fa"></i>Читати...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div><!--features_items-->

            </div>
        </div>
    </div>
</section>