<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="blog-post-area">
                    <h2 class="title text-center">Новини</h2>
                    <div class="single-blog-post">
                        <h3><?php echo $vars['titel'];?></h3>
                        <p><?php echo $vars['text_news'];?></p>
                        <div class="pager-area">
                            <ul class="pager pull-right">
                                <li><a href="#">Pre</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!--/blog-post-area-->
            </div>
        </div>
    </div>
</section>
