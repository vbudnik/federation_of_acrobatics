<section>
    <div class="container">
        <div class="features_items"><!--features_items-->
            <h2 class="title text-center">Новини</h2>
            <?php foreach ($vars['news'] as $news => $value): ?>
                <div class="col-sm-4">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <img src="../public/images/news/<?php echo $value['photo'];?>.jpg" alt="" />
                                <p><?php echo $value['titel'];?></p>
                                <div class="post-meta">
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                    </ul>
                                </div>
                                <a href="/page/<?php echo $value['id'];?>" class="btn btn-default add-to-cart"><i class="fa"></i>Читати...</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!--features_items-->
        <div class="clearfix">
            <?php echo $vars['pagination']; ?>
        </div>
    </div>
</section>