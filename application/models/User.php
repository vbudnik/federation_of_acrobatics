<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/15/18
 * Time: 8:42 PM
 */

namespace application\models;

use application\core\ConnectDb;
use \PDO;

class User
{
    public static function checkPassword($password){
        if(strlen($password) >= 6){
            return true;
        }
        return false;
    }

    public static function checkEmailExists($email){

        $db = Db::getConnection();

        $sql = 'select count(*) from user where email = :email';

        $result = $db->prepare($sql);

        $result->bindParam(':email', $email, PDO::PARAM_STR);

        $result->execute();

        if($result->fetchColumn()){
            return true;
        }
        return false;
    }

    public static function checkEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }

    public static function auth($userId){

        $_SESSION['user'] = $userId;
    }

    public static function checkLogin(){
        if(isset($_SESSION['user'])){
            return true;
        }
        return false;
    }


    public static function checkUserData($email, $password){

        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE email=:email AND password = :password';

        $result = $db->prepare($sql);

        $result->bindParam(':email', $email, PDO::PARAM_INT);
        $result->bindParam(':password', $password, PDO::PARAM_INT);
        $result->execute();



        $user = $result->fetch();

        if($user){
            return $user['id'];
        }
        return false;
    }

    public static function getUserId() {
        return $_SESSION['user'];
    }

    public static function registerUser($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO user (name, email, password)'
            .'VALUES (?, ?, ?);';

        $result = $db->prepare($sql);
        $result = $result->execute(array($options['name'], $options['email'], $options['password']));

        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getNameFromId($userId) {
        $db = Db::getConnection();

        $sql = 'select name from user where id=?';

        $result = $db->prepare($sql);
        $result->execute(array($userId));
        $row = $result->fetch();

        return $row['name'];
    }

    public static function getLastIdTrener(){
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT COUNT(ID) FROM trener';

        $result = $db->prepare($sql);
        $result->execute();
        $id = $result->fetchColumn();
        return $id;
    }

    public static function addTrener($option) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'INSERT INTO trener (name, description, photo) VALUES (?, ?, ?)';

        $result = $db->prepare($sql);
        $result = $result->execute(array($option['name'], $option['description'], $option['photo']));

        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getAllTrener($routs) {
        $max = 5;

        if (isset($routs['id'])){
            $start = ($routs['id'] - 1) * $max;
        } else {
            $start = 1;
        }

        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT * FROM trener ORDER BY id DESC LIMIT '.$start.','.$max.';';

        $result = $db->prepare($sql);
        $result->execute();

        $i = 0;
        $news = array();
        while ($row = $result->fetch()){
            $news[$i]['id'] = $row['id'];
            $news[$i]['name'] = $row['name'];
            $news[$i]['description'] = $row['description'];
            $news[$i]['photo'] = $row['photo'];
            $i++;
        }
        return $news;
    }

    public static function getInformationTrener($trenerId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "SELECT * FROM trener WHERE id=:user_id";

        $result = $db->prepare($sql);
        $result->execute(['user_id' => $trenerId]);
        $trener = $result->fetch(PDO::FETCH_ASSOC);
        return $trener;
    }

}